// ---------------------------------------------------------------------------
// Full 
// ---------------------------------------------------------------------------

#include <NewPing.h>
#include <BanziLib.h>

#define SONAR_NUM     3 // Number of sensors.
#define MAX_DISTANCE 200 // Maximum distance we want to ping for (in centimeters). Maximum sensor distance is rated at 400-500cm.

#define LEFT_SPEAKER 2 // the pin bound to the left speaker
#define RIGHT_SPEAKER 13 // the pin bound to the right speaker

//Create objects for execution
SonarBelt sonarBelt(4, 3, 7, 6, 12, 11, MAX_DISTANCE);
ObstacleOutput soundOutput(LEFT_SPEAKER, RIGHT_SPEAKER);

void setup() {
  Serial.begin(115200); // Open serial monitor at 115200 baud to see ping results.
}

void loop() {
  int leftResult, rightResult;
  sonarBelt.obstacleEstimation(&leftResult, &rightResult);
  serialPrint(leftResult, rightResult);
  soundOutput.displayObstacle(leftResult, rightResult);
}

void serialPrint(int leftResult, int rightResult) {
     Serial.print("left: ");
    Serial.print(leftResult); // Convert ping time to distance in cm and print result (0 = outside set distance range)
    Serial.println("cm");
    Serial.print("right: ");
    Serial.print(rightResult); // Convert ping time to distance in cm and print result (0 = outside set distance range)
    Serial.println("cm"); 
}
