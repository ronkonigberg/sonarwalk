//
//  BanziLib.h
//  Banzi Library - v1.0 - 12/30/2013
//  
//
// USES:
//  Serves as the framework for the SeeAgain Arduino Device.
//  Includes methods for detecting hazards and obstacles.
//
// CONSTRUCTOR:
//    
//
// SYNTAX:
//   
//

#ifndef _BanziLib_h
#define _BanziLib_h

#if defined(ARDUINO) && ARDUINO >= 100
#include <Arduino.h>
#else
#include <WProgram.h>
#include <pins_arduino.h>
#endif

#include <avr/io.h>
#include <avr/interrupt.h>

#include <NewPing.h>

class SonarBelt {
public:
	SonarBelt(uint8_t left_trigger_pin, uint8_t left_echo_pin,
			  uint8_t center_trigger_pin, uint8_t center_echo_pin,
			  uint8_t right_trigger_pin, uint8_t right_echo_pin,
			  int max_cm_distance);
			  
	void obstacleEstimation(int *left_result, int *right_result);
	
private:
	NewPing _left_sonar;
	NewPing _center_sonar;
	NewPing _right_sonar;
	
	int _max_distance;
	static uint8_t _num_samples;
};


class ObstacleOutput {
public:
	ObstacleOutput(uint8_t left_output_pin, uint8_t right_output_pin);
	void displayObstacle(int left_result, int right_result);
	
private:
	/*
	Beeps once in the requested speakers
	*/
	void beepOnce(bool beep_left, bool beep_right);
	void beep(unsigned int silence_duration, bool beep_left, bool beep_right);
	
	uint8_t _left_output_pin;
	uint8_t _right_output_pin;
	
	static unsigned int distanceToSilenceDuration(int distance);
	
	static unsigned int _center_beep_frequency; // centre beep frequency (HZ)
	static unsigned int _left_beep_frequency; // left beep frequency (HZ)
	static unsigned int _right_beep_frequency; // right beep frequency (HZ)
	
	static unsigned int _total_beep_duration;
	static unsigned int _one_beep_duration;
	
	static int _silence_distance_bins[];
	static unsigned int _silence_duration_bins[];
	static unsigned int _num_bins;
	
	static unsigned int _diff_delta;
};

#endif
