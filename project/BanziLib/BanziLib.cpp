// ---------------------------------------------------------------------------
// Created by Liz Miller - liz.miller11@gmail.com
//
// See "BanziLib.cpp" for purpose, syntax, version history, links, and more.
// ---------------------------------------------------------------------------

#include <BanziLib.h>

// SonarBelt
uint8_t SonarBelt::_num_samples = 5;

SonarBelt::SonarBelt(uint8_t left_trigger_pin, uint8_t left_echo_pin,
		uint8_t center_trigger_pin, uint8_t center_echo_pin,
		uint8_t right_trigger_pin, uint8_t right_echo_pin,
		int max_cm_distance):
	_left_sonar(left_trigger_pin, left_echo_pin, max_cm_distance),
	_center_sonar(center_trigger_pin, center_echo_pin, max_cm_distance),
	_right_sonar(right_trigger_pin, right_echo_pin, max_cm_distance),
	_max_distance(max_cm_distance) {}

void SonarBelt::obstacleEstimation(int *left_result, int *right_result) {
	unsigned int uS;
	unsigned int left_distance, center_distance, right_distance;
	
	uS = _left_sonar.ping_median(_num_samples);// Send ping, get ping time in microseconds (uS).
    left_distance = uS / US_ROUNDTRIP_CM; // convert to distance in cm.
	if (left_distance == 0)
		left_distance = _max_distance;
	
	uS = _center_sonar.ping_median(_num_samples);// Send ping, get ping time in microseconds (uS).
	center_distance = uS / US_ROUNDTRIP_CM; // convert to distance in cm.
	if (center_distance == 0)
		center_distance = _max_distance;
		
	uS = _right_sonar.ping_median(_num_samples);// Send ping, get ping time in microseconds (uS).
	right_distance = uS / US_ROUNDTRIP_CM; // convert to distance in cm.
	if (right_distance == 0)
		right_distance = _max_distance;
	
	if (left_distance < center_distance)
		*left_result = left_distance;
	else
		*left_result = center_distance;
	
	if (right_distance < center_distance)
		*right_result = right_distance;
	else
		*right_result = center_distance;	
}


// ObstacleOutput
#define DELAY_OFFSET 11

unsigned int ObstacleOutput::_center_beep_frequency = 880;
unsigned int ObstacleOutput::_left_beep_frequency = 261;
unsigned int ObstacleOutput::_right_beep_frequency = 2093;

unsigned int ObstacleOutput::_total_beep_duration = 500;
unsigned int ObstacleOutput::_one_beep_duration = 50;

int ObstacleOutput::_silence_distance_bins[] = {15, 30, 60};
unsigned int ObstacleOutput::_silence_duration_bins[] = {50, 150, 250};
unsigned int ObstacleOutput::_num_bins = 3;

unsigned int ObstacleOutput::_diff_delta = 10;

ObstacleOutput::ObstacleOutput(uint8_t left_output_pin, uint8_t right_output_pin):
		_left_output_pin(left_output_pin), _right_output_pin(right_output_pin) { 
	pinMode(_left_output_pin, OUTPUT);
	pinMode(_right_output_pin, OUTPUT);
}

unsigned int ObstacleOutput::distanceToSilenceDuration(int distance) {
	for (int i=0; i<_num_bins; ++i) {
		if (distance < _silence_distance_bins[i]) {
			return _silence_duration_bins[i];
		}
	}
	return _total_beep_duration;
}

void ObstacleOutput::displayObstacle(int left_result, int right_result) {
	bool beep_left=false, beep_right=false;
	unsigned int silence_duration = _total_beep_duration;
	
	// normalize the diff between left and right result according to the delta 
	if (right_result < left_result && left_result < (right_result + _diff_delta))
		left_result = right_result;
	if (left_result < right_result && right_result < (left_result + _diff_delta))
		right_result = left_result;
	
	if (left_result <= right_result && left_result < _silence_distance_bins[_num_bins-1]) {
		beep_left = true;
		silence_duration = distanceToSilenceDuration(left_result);
	}
	
	if (right_result <= left_result && right_result < _silence_distance_bins[_num_bins-1]) {
		beep_right = true;
		silence_duration = distanceToSilenceDuration(right_result);
	}
	
	beep(silence_duration, beep_left, beep_right);
}

void ObstacleOutput::beep(unsigned int silence_duration, bool beep_left, bool beep_right) {
  unsigned int num_beeps = _total_beep_duration / (_one_beep_duration + silence_duration);
  for (unsigned int i=0; i<num_beeps; ++i) {
    beepOnce(beep_left, beep_right);   
    delay(silence_duration);
  }
  
  // add minimal delay for a silent beep cycle
  if (num_beeps == 0)
	delay(0.2 * _total_beep_duration);
}

void ObstacleOutput::beepOnce(bool beep_left, bool beep_right) {
	uint8_t left_beep_bit, right_beep_bit;
	unsigned int beep_frequency;
	
	if (beep_left) {
		if (beep_right) {
			// centre beep
			left_beep_bit = HIGH;
			right_beep_bit = HIGH;
			beep_frequency = _center_beep_frequency;
		}
		else {
			// left beep
			left_beep_bit = HIGH;
			right_beep_bit = LOW;
			beep_frequency = _left_beep_frequency;
		}
	} else {
		if (beep_right) {
			// right beep
			left_beep_bit = LOW;
			right_beep_bit = HIGH;
			beep_frequency = _right_beep_frequency;
		}
		else {
			// don't beep
			delay(_one_beep_duration);
			return;
		}
	}
	
	// reference: 440hz = 2273 usec/cycle, 1136 usec/half-cycle
	unsigned int us = (500000 / beep_frequency) - DELAY_OFFSET;
	unsigned int rep = (_one_beep_duration * 500L) / (us + DELAY_OFFSET);
	for (int i=0; i<rep; ++i) {
		digitalWrite(_left_output_pin, left_beep_bit);
		digitalWrite(_right_output_pin, right_beep_bit);
		delayMicroseconds(us);
		digitalWrite(_left_output_pin, LOW);
		digitalWrite(_right_output_pin, LOW);
		delayMicroseconds(us);
	}
}
